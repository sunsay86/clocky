//
//  ClockView.swift
//  Clocky
//
//  Created by Александр Волков on 30.03.2021.
//

import SwiftUI
import Combine
import AVFoundation

struct ClockView : View {
    
    @State(initialValue: ClockModel(date: Date()))
    private var time: ClockModel
    
    @State
    private var timerSubscription: Cancellable? = nil
    
    private let hourPointerBaseRadius: CGFloat = 0.1
    
    private let secondPointerBaseRadius: CGFloat = 0.05
    
    private var audioPlayer: AVAudioPlayer!
    
    var body: some View {
        ZStack {
            Circle().stroke(Color.primary)
            ClockMarks()
            ClockNumber()
            ClockIndicator(type: .hour, time: time)
            ClockIndicator(type: .minute, time: time)
            ClockIndicator(type: .second, time: time)
        }
        .padding()
        .aspectRatio(1, contentMode: .fit)
        .onAppear { self.subscribe() }
        .onAppear(perform: {
                    playSound(sound: "sprayBug", type: "mp3")
                })
        .onDisappear { self.unsubscribe() }
    }
    
    private func subscribe() {
        timerSubscription =
            Timer.publish(every: 1, on: .main, in: .common)
            .autoconnect()
            .map(ClockModel.init)
            .assign(to: \.time, on: self)
    }
    
    private func unsubscribe() {
        timerSubscription?.cancel()
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ClockView()
    }
}
#endif
