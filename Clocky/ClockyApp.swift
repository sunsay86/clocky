//
//  ClockyApp.swift
//  Clocky
//
//  Created by Александр Волков on 30.03.2021.
//

import SwiftUI

@main
struct ClockyApp: App {
    var body: some Scene {
        WindowGroup {
            ClockView()
        }
    }
}
